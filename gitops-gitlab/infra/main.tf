module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "flux-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["<aws-region>a", "<aws-region>b", "<aws-region>c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = false
  single_nat_gateway = true
  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
  }
  tags = {
    Terraform = "true"
    Environment = "dev"
  }

}

resource "aws_ecr_repository" "go-app" {
  name = "<name_of_repo>"
}

resource "aws_ecr_registry_scanning_configuration" "go_scan" {
  scan_type = "BASIC"
  rule {
    scan_frequency = "SCAN_ON_PUSH"
    repository_filter {
      filter      = "*"
      filter_type = "WILDCARD"
    }
  }
}

module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "~> 20.0"
  cluster_name = "eks-cluster"
  cluster_version = "1.29"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  cloudwatch_log_group_retention_in_days = 7
  cloudwatch_log_group_class = "INFREQUENT_ACCESS"
  cluster_enabled_log_types = ["api"]
  vpc_id                   = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets
  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    aws-ebs-csi-driver = {
      most_recent    = true
    }
    eks-pod-identity-agent = {
      most_recent    = true
    }
    vpc-cni = {
      most_recent    = true
    }
  }
  eks_managed_node_group_defaults = {
    ami_type               = "AL2_x86_64"
    disk_size              = 50
    instance_types = ["t3.large"]
    capacity_type = "SPOT"
    update_config = {
      max_unavailable_percentage = 100
    }
  }
  eks_managed_node_groups = {
    ps-cluster-sample = {
      min_size = 1
      desired_size = 1
      max_size = 4
      instance_types = ["t3.large"]
      capacity_type  = "SPOT"
    }
  }
  enable_cluster_creator_admin_permissions = false
  access_entries = {
    admin_sso = {
      kubernetes_groups = []
      principal_arn     = "<arn of the role or the user to which you want to grant admin privileges>"
      policy_associations = {
        ClusterAdmin = {
          policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSClusterAdminPolicy"
          access_scope = {
            type       = "cluster"
          }
        }
        EKSAdmin = {
          policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSAdminPolicy"
          access_scope = {
            type       = "cluster"
          }
        }
      }
    }
  }
}

resource "null_resource" "update_kubeconfig" {
  triggers = {
    eks_cluster_id = module.eks.cluster_id
  }

  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name ${module.eks.cluster_name} --region ${var.aws_region}"
  }
}

resource "tls_private_key" "flux" {
  depends_on = [module.eks, module.vpc, null_resource.update_kubeconfig]
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

data "gitlab_project" "this" {
  path_with_namespace = "${var.gitlab_group}/${var.gitlab_project}"
}

resource "gitlab_deploy_key" "this" {
  depends_on = [module.eks, module.vpc]
  project  = data.gitlab_project.this.id
  title    = "Flux"
  key      = tls_private_key.flux.public_key_openssh
  can_push = true
}

resource "flux_bootstrap_git" "this" {
  depends_on = [gitlab_deploy_key.this, module.eks, module.vpc, null_resource.update_kubeconfig]
  path = "clusters/${module.eks.cluster_name}"
  components_extra = ["image-reflector-controller","image-automation-controller"]
}

locals {
  yaml_files = fileset("${path.module}/config_files", "*.yml")

  yaml_content = { for file in local.yaml_files : file => file("${path.module}/config_files/${file}") }
}

resource "gitlab_repository_file" "yaml_files" {
  depends_on = [flux_bootstrap_git.this, null_resource.update_kubeconfig]
  for_each = local.yaml_content
  project = data.gitlab_project.this.id // Use the project ID of an existing project or gitlab_project.example_project.id if you created a new project
  file_path = "clusters/${module.eks.cluster_name}/${each.key}"
  content   = base64encode(each.value)
  commit_message = "added flux image configs"
  branch    = "main"
}

locals {
  app_files = fileset("${path.module}/app_files", "*.yml")

  app_content = { for file in local.app_files : file => file("${path.module}/app_files/${file}") }
}

resource "gitlab_repository_file" "app_deployment_files" {
  for_each = local.app_content
  project = data.gitlab_project.this.id // Use the project ID of an existing project or gitlab_project.example_project.id if you created a new project
  file_path = "app/${each.key}"
  content   = base64encode(each.value)
  commit_message = "added app configs"
  branch    = "main"
}



