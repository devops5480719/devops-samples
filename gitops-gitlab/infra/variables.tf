variable "gitlab_group" {
  type = string
  default = ""
}

variable "gitlab_project" {
  type = string
  default = ""
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}