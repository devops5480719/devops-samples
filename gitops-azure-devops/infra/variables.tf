variable "pat_token" {
  type = string
  default = ""
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}